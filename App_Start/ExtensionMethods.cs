﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AIDL
{
    public static class ExtensionMethods
    {
        private const string V = "FGH345645789@!#$%^&*";

        public static string ToSuccess(this string message)
        {
            return string.Format("showMessage('{0}',true);", message.Replace("'", "").Replace("\r", "").Replace("\n", "\\"));
        }

        public static string ToError(this string message)
        {
            return string.Format("showMessage('{0}',false);", message.Replace("'", "").Replace("\r", "").Replace("\n", "\\"));
        }
        public static string ToExceptionError(this string message)
        {
            //TODO: Log Error here
            return string.Format("showMessage('{0}',false);", message.Replace("'", "").Replace("\r", "").Replace("\n", "\\"));
        }

        public static string ToEncrypt(this string clearText)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(V, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText.Replace("/", "check").Replace("+", "plus").Replace("?", "question");
        }

        public static string ToDecrypt(this string cipherText)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText.Replace("check", "/").Replace("plus", "+").Replace("question", "?"));
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(V, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static T ToEnum<T>(this int value)
        {
            var name = Enum.GetName(typeof(T), value);
            return name.ToEnum<T>();
        }

    }
}