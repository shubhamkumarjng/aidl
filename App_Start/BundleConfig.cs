﻿using System.Web;
using System.Web.Optimization;

namespace AIDL
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            #region JQuery



            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/HomeLayout").Include(
                      "~/Scripts/popper/popper.min.js",
                      "~/Scripts/bootstrap/bootstrap.min.js",
                      "~/Scripts/wow/wow.min.js",
                      "~/Scripts/owl-carousel/owl.carousel.min.js",
                      "~/Scripts/jquery-easing/jquery.easing.min.js",
                      "~/Scripts/jquery.nicescroll.js",
                      "~/Scripts/pgwmodal.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/Custom").Include("~/Scripts/custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/player/audio").Include(
                        "~/Scripts/audioplayer/audioplayer.js",
                        "~/Scripts/viewerjs/viewer.min.js",
                        "~/Scripts/viewerjs/jquery-viewer.js",
                        "~/Scripts/viewerjs/main.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/Js_Developer").Include("~/Scripts/js_developer.js"));

            #endregion

            #region css
            //css
            bundles.Add(new StyleBundle("~/Content/Css/HomeLayout").Include(
                      //"~/Content/css/bootstrap.min.css",
                      "~/Content/css/font-awesome.min.css",
                      "~/Content/css/animate/animate.min.css",
                      "~/Content/css/owl-carousel/owl.carousel.min.css",
                      "~/Content/css/owl-carousel/owl.theme.default.min.css",
                      "~/Content/css/pgwmodal.min.css",
                      "~/Content/css/style.css"));

            bundles.Add(new StyleBundle("~/Content/Css/Player/AudioViewer").Include(
                "~/Content/css/audioplayer/audioplayer.css",
                "~/Content/css/viewerjs/viewer.min.css"
                ));
            bundles.Add(new StyleBundle("~/Content/Css/ContentPages").Include(
                "~/Content/css/content_pages.css"
                ));
            bundles.Add(new StyleBundle("~/Content/Css/css_developer").Include(
                "~/Content/css/css_developer.css"
                ));


            //Admin 
            bundles.Add(new StyleBundle("~/Areas/Admin/css/preloadlayout").Include(
                 "~/Areas/Admin/css/bootstrap.min.css",
                 "~/Areas/Admin/css/skin-green.min.css",
                 "~/Areas/Admin/css/AdminLTE.min.css",
                "~/Areas/Admin/css/designer_css_developer.min.css"
                ));
            bundles.Add(new StyleBundle("~/Areas/Admin/css/layout").Include(
                 "~/Areas/Admin/css/font-awesome.min.css",
                 "~/Areas/Admin/css/select2.min.css",
                 "~/Areas/Admin/css/datepicker.min.css",
                 "~/Areas/Admin/css/dataTables.bootstrap.min.css",
                "~/Areas/Admin/css/responsive.bootstrap.min.css",
                "~/Areas/Admin/css/buttons.dataTables.min.css",
                 "~/Areas/Admin/css/desCXZADigner.min.css",
                 "~/Areas/Admin/css/flat/_all.css",
                 "~/Areas/Admin/css/fileinput.min.css",
                "~/Areas/Admin/css/pgwmodal.min.css",
               "~/Areas/Admin/css/css_developer.css"
                ));
            bundles.Add(new StyleBundle("~/Areas/Admin/css/lightgallery").Include("~/Areas/Admin/css/lightgallery.min.css"));
            #endregion
        }
    }
}
