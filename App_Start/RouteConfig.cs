﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AIDL
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "About-Us",
                url: "about-us",
                defaults: new { controller = "Home", action = "About" },
                namespaces: new[] { "AIDL.Controllers" }
            );


            //routes.MapRoute(
            //    name: "Services",
            //    url: "services",
            //    defaults: new { controller = "Home", action = "Services" }
            //);

            routes.MapRoute(
                name: "Services",
                url: "services",
                defaults: new { Controller = "Home", action = "Services" },
                namespaces: new[] { "AIDL.Controllers" }
            );
            routes.MapRoute(
                name: "Fees",
                url: "fees",
                defaults: new { Controller = "Home", action = "Fees" },
                namespaces: new[] { "AIDL.Controllers" }
            );

            routes.MapRoute(
                name: "Contact-Us",
                url: "contact-us",
                defaults: new { controller = "Home", action = "Contact" },
                namespaces: new[] { "AIDL.Controllers" }
            );

            routes.MapRoute(
                name: "Privacy-Policy",
                url: "privacy-policy",
                defaults: new { controller = "Home", action = "PrivacyPolicy" },
                namespaces: new[] { "AIDL.Controllers" }
            );

            routes.MapRoute(
                name: "Lessons",
                url: "{language}/lessons/{id}",
                defaults: new { controller = "Lessons", action = "Index", language = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "AIDL.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "AIDL.Controllers" }
            );
        }
    }
}
