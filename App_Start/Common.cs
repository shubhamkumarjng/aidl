﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIDL
{
    public static class Enums
    {
        public enum LessonLanguages
        {
            Punjabi,
            Hindi,
            Bengali,
            Pakistani
        }

        public static string GetLessonLanguages(int enumValue)
        {
            return ((LessonLanguages)enumValue).ToString();
        }

    }
}