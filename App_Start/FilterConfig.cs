﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AIDL
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
        public class CustomAuthorize : AuthorizeAttribute
        {
            public string Url { get; set; }

            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Result = new RedirectResult(Url + "?returnUrl=" + filterContext.HttpContext.Request.Url.PathAndQuery);
            }

            public override void OnAuthorization(AuthorizationContext filterContext)
            {
                if (this.AuthorizeCore(filterContext.HttpContext))
                {
                    if (filterContext.HttpContext.User.IsInRole("admin"))
                    {
                        if (filterContext.HttpContext.Session["AdminRole"] == null)
                        {
                            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
                            FormsIdentity fi;
                            fi = (FormsIdentity)filterContext.HttpContext.User.Identity;
                            string[] ud = fi.Ticket.UserData.Split(';');
                            Session["AdminRole"] = ud[0].ToString();
                            Session["AdminId"] = int.Parse((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                            Session["AdminUsername"] = ud[3];
                        }
                    }
                    else if (filterContext.HttpContext.User.IsInRole("Affiliate"))
                    {
                        if (filterContext.HttpContext.Session["AffiliateUserId"] == null)
                        {
                            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
                            FormsIdentity fi;
                            fi = (FormsIdentity)filterContext.HttpContext.User.Identity;
                            string[] ud = fi.Ticket.UserData.Split(';');
                            Session["AffiliateUserId"] = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                            Session["AffiliateName"] = ud[2];
                            Session["AffiliateEmailAddress"] = (ud.Length >= 3 ? "" : ud[3]);
                        }
                    }
                    base.OnAuthorization(filterContext);
                }
                else
                {
                    this.HandleUnauthorizedRequest(filterContext);
                }
            }
        }
    }
}
