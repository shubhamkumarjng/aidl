﻿/*Disable Right Click*/
$(document).ready(function () {
    $(document).bind("contextmenu", function (e) {
        return false;
     });
});

/*Mesage popup*/
function showMessage(msg, bool, timeout) {
    timeout = (typeof timeout !== 'undefined') ? timeout : 15000;
    if (bool === true) {
        $('#div_msg').html('<div class="alert-success">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#DFF0D8" })
    }
    else {
        $('#div_msg').html('<div style="color: #dc0400;">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#ffebeb" })
    }
    setTimeout(function () {
        $.pgwModal('close');
    }, timeout);
}