﻿using System;
using System.Drawing.Imaging;

namespace CompressImageCore
{
    public class CompressImage
    {
        public void CompressImageQualityLevel(string path, string encodedValue)
        {
            CompressImageQualityLevel(path, encodedValue, null);
        }

        public void CompressImageQualityLevel(string path, string encodedValue, System.Drawing.Image img)
        {
            if (img != null)
            {
                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                // EncoderParameter object in the array.  
                Encoder myEncoder =
                    Encoder.Quality;
                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, encodedValue);
                myEncoderParameters.Param[0] = myEncoderParameter;
                img.Save(path, jpgEncoder, myEncoderParameters);
            }
            else
            {
                System.Drawing.Bitmap bmp1 = new System.Drawing.Bitmap(path);

                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                // EncoderParameter object in the array.  
                Encoder myEncoder =
                    Encoder.Quality;
                EncoderParameters myEncoderParameters = new EncoderParameters(1);
                System.Drawing.Bitmap newBitmap = new System.Drawing.Bitmap(bmp1);
                bmp1.Dispose();
                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, encodedValue);
                myEncoderParameters.Param[0] = myEncoderParameter;
                newBitmap.Save(path, jpgEncoder, myEncoderParameters);
            }
        }
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }
}
