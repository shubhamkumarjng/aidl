﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AIDL.CustomHelper
{
    public static class CustomHelper
    {
        public static MvcHtmlString FormatDate(this HtmlHelper htmlHelper, DateTime? dateTime)
        {
            return MvcHtmlString.Create((dateTime == null ? "" : (dateTime ?? new DateTime()).ToString("yyyy/MM/dd")));
        }
        public static MvcHtmlString FormatDateTime(this HtmlHelper htmlHelper, DateTime? dateTime)
        {
            return MvcHtmlString.Create((dateTime == null ? "" : (dateTime ?? new DateTime()).ToString("yyyy/MM/dd hh:mm tt")));
        }
    }
}