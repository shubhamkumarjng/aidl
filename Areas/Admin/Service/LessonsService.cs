﻿using AIDL.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AIDL.Areas.Admin.Service
{
    public class LessonsService : Shared.Service.BaseService
    {
        public tblLessons lesson { get; set; }

        protected UnitOfWork unitOfWork { get; private set; }
        public LessonsService()
        {
            unitOfWork = new UnitOfWork();
        }

        public void AddEditLesson()
        {
            try
            {
                string fileName = "";
                if (lesson.Id == 0)
                {
                    lesson.created_on = DateTime.Now;
                    lesson.status = true;

                    fileName = $"{Enums.GetLessonLanguages(lesson.lesson_type)}-page-{lesson.page_number}" + (lesson.page_part == 0 || lesson.page_part == null ? "" : "-part-" + lesson.page_part).ToLower();
                    lesson.audio = fileName + Path.GetExtension(lesson.audio_file.FileName);
                    lesson.image = fileName + Path.GetExtension(lesson.image_file.FileName);
                    unitOfWork.tblLessonsRepo.Add(lesson);
                    unitOfWork.tblLessonsRepo.Save();
                    uploadFiles();
                    Message = "Lesson has been added successfully";
                }
                else
                {
                    lesson.modified_on = DateTime.Now;

                    fileName = $"{Enums.GetLessonLanguages(lesson.lesson_type)}-page-{lesson.page_number}" + (lesson.page_part == 0 || lesson.page_part == null ? "" : "-part-" + lesson.page_part) + System.IO.Path.GetExtension(lesson.audio).ToLower();
                    lesson.audio = fileName + Path.GetExtension(lesson.audio_file.FileName);
                    lesson.image = fileName + Path.GetExtension(lesson.image_file.FileName);
                    unitOfWork.tblLessonsRepo.Update(lesson, lesson.Id);
                    unitOfWork.tblLessonsRepo.Save();
                    uploadFiles();
                    Message = "Lesson has been updated successfully";
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = ex.Message.ToExceptionError();
            }
        }

        private void uploadFiles()
        {
            string uploadFolders = HttpContext.Current.Server.MapPath("~/Uploads");
            if (!Directory.Exists(uploadFolders + "//" + Enums.GetLessonLanguages(lesson.lesson_type)))
            {
                Directory.CreateDirectory(uploadFolders + "//" + Enums.GetLessonLanguages(lesson.lesson_type));
                Directory.CreateDirectory(uploadFolders + "//audio");
                Directory.CreateDirectory(uploadFolders + "//image");
            }
            lesson.audio_file.SaveAs(HttpContext.Current.Server.MapPath("~/Uploads/" + Enums.GetLessonLanguages(lesson.lesson_type) + "/audio/" + lesson.audio));
            lesson.image_file.SaveAs(HttpContext.Current.Server.MapPath("~/Uploads/" + Enums.GetLessonLanguages(lesson.lesson_type) + "/image/" + lesson.image));
        }
    }
}
