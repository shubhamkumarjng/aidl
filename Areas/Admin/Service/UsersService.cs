﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AIDL.Areas.Admin.Shared.Service;
using AIDL.Repository;
using AIDL.Shared.Service;

namespace AIDL.Areas.Admin.Service
{
    public class UsersService : Shared.Service.BaseService
    {
        public tblUsers user { get; set; }

        protected UnitOfWork unitOfWork { get; private set; }
        public UsersService()
        {
            unitOfWork = new UnitOfWork();
        }

        public void AddEditUser()
        {
            try
            {
                user.status = true;
                //add user
                if (user.Id == 0)
                {
                    user.password = PsswordGeneratorService.GeneratePassword(true, false, true, false, false, 6);
                    user.created_on = DateTime.Now;
                    unitOfWork.tblUsersRepo.Add(user);
                    unitOfWork.tblUsersRepo.Save();
                    Message = "User has been registered successfully.".ToSuccess();
                    //send email to user
                    clsEmailTemplateService objEmailTemplateService = clsEmailTemplateService.Instance;
                    System.Collections.Hashtable objHashTable = new System.Collections.Hashtable();
                    objHashTable.Add("#name#", user.name);
                    objHashTable.Add("#username#", user.email);
                    objHashTable.Add("#password#", user.password);
                    string body = objEmailTemplateService.GetEmailTemplate("signup", objHashTable);
                    (clsSendEmailService.Instance).SendEmail(new string[] { user.email }, null, null, body, "Sign Up Successfully");
                }
                else
                {
                    user.modified_on = DateTime.Now;
                    unitOfWork.tblUsersRepo.Update(user, user.Id);
                    unitOfWork.tblUsersRepo.Save();
                    Message = "User has been updated successfully.".ToSuccess();
                }
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = ex.Message.ToError();
            }
        }
        public void ChangeStatusById(int id)
        {
            try
            {
                var itemToRemove = unitOfWork.tblUsersRepo.FindBy(x => x.Id == id).FirstOrDefault();
                if (itemToRemove != null)
                {
                    unitOfWork.tblUsersRepo.context.Configuration.ValidateOnSaveEnabled = false;
                    itemToRemove.status = !itemToRemove.status;
                    unitOfWork.tblUsersRepo.Update(itemToRemove, itemToRemove.Id);
                }
                unitOfWork.tblUsersRepo.Save();
                IsSuccess = true;
                Message = "Status has been changed successfully.".ToSuccess();
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = ex.Message.ToError();
            }
        }



    }
}