﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIDL.Areas.Admin.Shared.Service
{
    public abstract class BaseService
    {
        public bool IsSuccess { get; set; } = true;
        public string Message { get; set; }
    }
}