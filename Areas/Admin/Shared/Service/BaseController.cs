﻿using AIDL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AIDL.Areas.Admin.Shared.Service
{
    public abstract partial class BaseController : Controller
    {
        public UnitOfWork unitOfWork;

        public BaseController()
        {
            unitOfWork = new UnitOfWork();
        }

        public String MessageTempData
        {
            get { return TempData["Msg"] == null ? String.Empty : TempData["Msg"].ToString(); }
            set { TempData["Msg"] = value; }
        }

        public String MessageViewBag
        {
            get { return ViewBag.Msg == null ? String.Empty : ViewBag.Msg.ToString(); }
            set { ViewBag.Msg = value; }
        }
    }

}