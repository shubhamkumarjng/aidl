﻿$(document).ready(function () {
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
    //Drop Down Start
    $("select[SearchSelect='true']").select2({ width: '100%' });

    //focus next    
    $('select').on('select2:select', function (evt) {
        $(this).parents('div').next('div').eq(0).find('input').eq(0).focus().select();
    });
    //open on focus
    $(document).on('focus', '.select2', function () {
        $(this).siblings('select').select2('open');
    });

    //Drop Down End
    //Date Picker Start
    var FromEndDate = new Date();
    $('input[calendar="date"]').datepicker({
        format: 'yyyy/mm/dd',
        endDate: FromEndDate,
        autoclose: true
    });
    $('input[calendar="enabledDates"]').datepicker({
        format: 'yyyy/mm/dd',
        startDate: FromEndDate,
        autoclose: true
    });
    //Date Picker End

    //Datatable Start
    $("table[id='datatable']").DataTable({
        responsive: true,
        "order": [],
        "columnDefs":   [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: -1 }
        ]
    });
    //Datatable End
})
function showMessage(msg, bool, timeout) {
    timeout = (typeof timeout !== 'undefined') ? timeout : 7000;

    if (bool === true) {
        $('#div_msg').html('<div>' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#DFF0D8" })
    }
    else {
        $('#div_msg').html('<div style="color: #dc0400;">' + msg + '</div>');
        $.pgwModal({
            target: '#div_msg',
            titleBar: false
        });
        $(".pm-content").css({ "background": "#ffebeb" })
    }
    setTimeout(function () {
        $.pgwModal('close');
    }, timeout);
}

function Cancel_Click() {
    window.location = window.location.href;
}

/*Loader Ajax Event Start*/
$(document).ajaxSend(function () {
    $('#preloader').fadeIn('slow').children().fadeIn('slow');
});

$(document).ajaxSuccess(function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});``

$(document).ajaxComplete(function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
});
/*Loader Ajax Event End*/
/*Loader Show Hide End*/

/*Cancel Button click start*/
/*Cancel Button click end*/

/*Prevent validation on Key Up start*/
$(function () {
    $.validator.setDefaults({
        onkeyup: false
    });
});

/*Prevent validation on Key Up end*/
/*Loader on page Load and onbeforeunload*/;
$(window).load(function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
});
window.onload = function () {
    $('#preloader').fadeOut('slow').children().fadeOut('slow');
}
window.onbeforeunload = function () { $('#preloader').fadeIn('slow').children().fadeIn('slow'); }
/*Loader on page Load and Post Backing End*/
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}