﻿using AIDL.Areas.Admin.Service;
using AIDL.Repository;
using AIDL.Shared.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AIDL.Areas.Admin.Controllers
{
    public class LessonsController : BaseController
    {
        private readonly LessonsService _lessonsService;
        public LessonsController()
        {
            _lessonsService = new LessonsService();
        }
        // GET: Admin/Lessonss
        public ActionResult Index()
        {
            return View(unitOfWork.tblLessonsRepo.GetAll().AsEnumerable());
        }

        [HttpGet]
        public ActionResult Add(int? Id)
        {
            tblLessons lesson = new tblLessons();
            if ((Id ?? 0) != 0)
            {
                lesson = unitOfWork.tblLessonsRepo.Get(Id ?? 0);
            }
            return View(lesson);
        }
        [HttpPost]
        public ActionResult Add(tblLessons lesson)
        {
            if (ModelState.IsValid)
            {
                _lessonsService.lesson = lesson;
                _lessonsService.AddEditLesson();
                MessageTempData = _lessonsService.Message;
                return RedirectToAction("Index");
            }
            MessageViewBag = "One or more error has been occured. Please contact customer care.";
            return View();
        }

        
    }
}