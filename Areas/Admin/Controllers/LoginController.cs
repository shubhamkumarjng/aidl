﻿using AIDL.Areas.Admin.Shared.Service;
using AIDL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AIDL.Areas.Admin.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin") || User.IsInRole("Staff"))
                {
                    FormsIdentity fi;
                    fi = (FormsIdentity)User.Identity;
                    string[] ud = fi.Ticket.UserData.Split(';');
                    Session["AdminRole"] = ud[0];
                    Session["AdminId"] = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
                    Session["AdminUsername"] = ud[2];
                    Session["AdminName"] = ud[3];
                    return RedirectToAction("Index", "Home");
                }
            }
            if (Request.QueryString["returnUrl"] != null)
            {
                TempData["retUrl"] = Request.QueryString["returnUrl"].ToString();
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(tblAdminUsers objAdminUser)
        {
            var AdminUser = unitOfWork.tblAdminUsersRepo.FindBy(x => x.username == objAdminUser.username && x.password == objAdminUser.password && x.status == true).Select(x => x).FirstOrDefault();
            if (AdminUser != null)
            {
                if (AdminUser.password == objAdminUser.password)
                {
                    HttpCookie cookie = HttpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                    if (cookie == null)
                    {
                        cookie = new HttpCookie(FormsAuthentication.FormsCookieName);

                        HttpContext.Response.Cookies.Add(cookie);
                    }
                    string UserData = AdminUser.role + ";" + AdminUser.Id + ";" + AdminUser.username + ";" + AdminUser.name;
                    string UserName = AdminUser.username;
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddDays(1), true, UserData, FormsAuthentication.FormsCookiePath);
                    cookie.Value = FormsAuthentication.Encrypt(ticket);
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Response.Cookies.Set(cookie);
                    HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, UserData.Split(';'));
                    if (TempData["retUrl"] != null)
                    {
                        Response.Redirect(TempData["retUrl"].ToString(), true);
                    }
                    else
                        Response.Redirect("~/Admin/", true);
                }
                else
                {
                    MessageViewBag = "showMessage('Invalid username or password!',false);";
                }
            }   
            else
            {
                MessageViewBag = "showMessage('Invalid username or password!',false);";
            }
            return View();
        }

        public void SignOut()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Admin/Login", true);
        }
    }
}