﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIDL.Areas.Admin.Shared.Service;
using AIDL.Repository;
using AIDL.Areas.Admin.Service;
using static AIDL.FilterConfig;

namespace AIDL.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class UsersController : BaseController
    {
        private readonly UsersService _usersService;
        public UsersController()
        {
            _usersService = new UsersService();
        }
        // GET: Admin/Users
        public ActionResult Index()
        {
            return View(unitOfWork.tblUsersRepo.GetAll().AsEnumerable());
        }

        [HttpGet]
        public ActionResult RegisterUser(int? Id)

        {
            tblUsers user = new tblUsers();
            user.status = true;
            ViewBag.BoxTitle = "Create User";
            if (Id != null && Id != 0)
            {
                ViewBag.BoxTitle = "Update User";
                user = unitOfWork.tblUsersRepo.Get(Id ?? 0);
            }
            return View(user);
        }

        [HttpPost]
        public ActionResult RegisterUser(tblUsers user)
        {
            if (string.IsNullOrEmpty(user.password))
            {
                ModelState.Remove("password");
            }
            if (!ModelState.IsValid)
            {
                MessageViewBag = "One or more fields are invalid.".ToError();
                return View();
            }
            _usersService.user = user;
            _usersService.AddEditUser();
            if (_usersService.IsSuccess)
            {
                ModelState.Clear();
            }

            MessageTempData = _usersService.Message;
            if (_usersService.IsSuccess)
            {
                return RedirectToAction("Index", "Users");
            }
            return View();
        }

        public JsonResult ChangeStatusById(int id)
        {
            if (id != 0)
            {
                _usersService.ChangeStatusById(id);
            }
            return Json(id.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult isEmailExists(string email, int Id)
        {
            bool isExists = unitOfWork.tblUsersRepo.FindBy(x => x.email == email && x.Id != Id).Any();
            if (isExists)
            {
                return Json("Email/Username is already exists", JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}