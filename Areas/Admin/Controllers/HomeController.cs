﻿using AIDL.Areas.Admin.Shared.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static AIDL.FilterConfig;

namespace AIDL.Areas.Admin.Controllers
{
    [CustomAuthorize(Url = "~/admin/login", Roles = "Admin ,Staff")]
    public class HomeController : BaseController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}