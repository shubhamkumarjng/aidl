﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AIDL.Models;
using AIDL.Repository;
using AIDL.Service;
using AIDL.Shared.Service;

namespace AIDL.Controllers
{
    public class LoginController : BaseController
    {
        UserService userService = new UserService();
        // GET: Login
        public ActionResult Index()
        {
            UserLogin userLogin = new UserLogin();

            if (Request.Cookies["..."] != null)
            {
                string e = Request.Cookies["..."].Values["email"].ToDecrypt();
                string p = Request.Cookies["..."].Values["password"].ToDecrypt();
                userLogin.email = e;
                userLogin.password = p;
            }
            return View(userLogin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserLogin user)
        {
            tblUsers loggedInUser = new tblUsers();
            userService.userLoginModel = user;
            try
            {
                loggedInUser = userService.LoginUser();
                if (!userService.IsSuccess)
                {
                    MessageViewBag = userService.Message;
                }
                else
                {
                    return createCookieAndRedirectToUserHome(loggedInUser, user.RememberMe);
                }
            }
            catch (Exception ex)
            {
                MessageViewBag = ex.Message.ToExceptionError();
            }
            return View();
        }
        private ActionResult createCookieAndRedirectToUserHome(tblUsers user, bool rememberMe)
        {
            try
            {
                if (rememberMe)
                {
                    HttpContext.Request.Cookies.Remove("...");
                    HttpCookie Cookie = new HttpCookie("...");
                    Cookie.Values.Add("email", user.email.ToEncrypt());
                    Cookie.Values.Add("password", user.password.ToEncrypt());
                    Cookie.Expires = DateTime.Now.AddDays(365);
                    Response.Cookies.Add(Cookie);
                }
                HttpCookie cookie = HttpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                if (cookie == null)
                {
                    cookie = new HttpCookie(FormsAuthentication.FormsCookieName);

                    HttpContext.Response.Cookies.Add(cookie);
                }
                string UserData = "User" + ";" + user.Id + ";" + user.name + ";" + user.email;
                string UserName = user.email;
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddDays(1), true, UserData, FormsAuthentication.FormsCookiePath);
                cookie.Value = FormsAuthentication.Encrypt(ticket);
                cookie.Expires = DateTime.Now.AddDays(30);
                HttpContext.Response.Cookies.Set(cookie);
                HttpContext.User = new GenericPrincipal(HttpContext.User.Identity, UserData.Split(';'));

                if (TempData["RetUrl"] != null)
                {
                    if (!Convert.ToString(TempData["RetUrl"]).Split('/').Last().ToLower().Contains("signout"))
                        Response.Redirect(Convert.ToString(TempData["RetUrl"]), true);
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToExceptionError();
            }
            return RedirectToAction("Index","Home");
        }

        [Authorize]
        public ActionResult LogOut()
        {
            clsAuthentication.SignOutUser();
            return RedirectToAction("Index", "Home");
        }

    }
}