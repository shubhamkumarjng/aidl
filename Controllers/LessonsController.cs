﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIDL.Repository;
using AIDL.Service;
using AIDL.Shared.Service;
using AIDL.Models;
namespace AIDL.Controllers
{
    public class LessonsController : BaseController
    {
        LessonService lessonService = new LessonService();
        // GET: Lessons
        public ActionResult Index(string language, int? id)
        {  
            ViewBag.Title = clsUtils.FirstCharToUpper(language) + " Lessons";
            ViewBag.Language = clsUtils.FirstCharToUpper(language) ;
            tblLessons lesson = new tblLessons();
            LessonModel lessonModel = new LessonModel();
            PagedList.IPagedList<tblLessons> pageList = null;
            if (clsAuthentication.IsAuthenticated)
            {
                lesson = lessonService.getLesson(language.ToEnum<Enums.LessonLanguages>(), id);
                if (lesson != null)
                {
                    lessonModel.id = lesson.Id;
                    lessonModel.title = $"Page No. {lesson.page_number}" + ((lesson.page_part ?? 0) == 0 ? "" : $" - Part {lesson.page_part}");
                    lessonModel.audio = $"/Uploads/{language}/audio/{lesson.audio}";
                    lessonModel.image = $"/Uploads/{language}/image/{lesson.image}";
                    ViewBag.PageList = lessonService.geneatePagelist(language.ToEnum<Enums.LessonLanguages>(), (id ?? 1));
                }

            }
            if (lessonModel.id == 0)
            {
                lessonModel.id = 0;
                lessonModel.title = "Sample Page No. 22";
                lessonModel.audio = "/Uploads/Punjabi/audio/sample-page-22.mp4";
                lessonModel.image = "/Uploads/Punjabi/image/sample-page-22.jpg";
            }

            return View(lessonModel);
        }

        private void generatePagination(string lesson)
        {

        }

    }
}