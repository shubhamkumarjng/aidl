﻿using AIDL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AIDL.Service;
using AIDL.Areas.Admin.Shared.Service;

namespace AIDL.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()
        { }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(tblContactUs tblContactUs)
        {
            SendContactUsEmail(tblContactUs);
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult Fees()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(tblContactUs tblContactUs)
        {
            SendContactUsEmail(tblContactUs);
            return RedirectToAction("Contact");
        }

        private void SendContactUsEmail(tblContactUs tblContactUs)
        {
            ContactUsService contactUsService = new ContactUsService();
            contactUsService.contactUs = tblContactUs;
            contactUsService.sendContactUsEmail();
            MessageTempData = contactUsService.Message;
        }
    }
}