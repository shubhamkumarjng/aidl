﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIDL.Repository
{
    public class UnitOfWork
    {
        private AIDLEntities context = new AIDLEntities();

        private IGenericRepository<tblContactUs> _tblContactUsRepo;
        public IGenericRepository<tblContactUs> tblContactUsRepo
        {
            get
            {
                return _tblContactUsRepo ?? (_tblContactUsRepo = new GenericRepository<tblContactUs>(context));
            }
        }

        private IGenericRepository<tblAdminUsers> _tblAdminUsersRepo;
        public IGenericRepository<tblAdminUsers> tblAdminUsersRepo
        {
            get
            {
                return _tblAdminUsersRepo ?? (_tblAdminUsersRepo = new GenericRepository<tblAdminUsers>(context));
            }
        }

        private IGenericRepository<tblUsers> _tblUsersRepo;
        public IGenericRepository<tblUsers> tblUsersRepo
        {
            get
            {
                return _tblUsersRepo ?? (_tblUsersRepo = new GenericRepository<tblUsers>(context));
            }
        }


        private IGenericRepository<tblLessons> _tblLessonsRepo;
        public IGenericRepository<tblLessons> tblLessonsRepo
        {
            get
            {
                return _tblLessonsRepo ?? (_tblLessonsRepo = new GenericRepository<tblLessons>(context));
            }
        }

    }
}