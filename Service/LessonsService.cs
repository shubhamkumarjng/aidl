﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AIDL.Repository;
using AIDL.Shared.Service;
using PagedList;

namespace AIDL.Service
{
    public class LessonService : BaseService
    {
        public tblLessons lesson { get; set; }

        protected UnitOfWork unitOfWork { get; private set; }
        public LessonService()
        {
            unitOfWork = new UnitOfWork();
        }

        public tblLessons getLesson(Enums.LessonLanguages language, int? id)
        {
            tblLessons lesson = new tblLessons();
            try
            {
                lesson = unitOfWork.tblLessonsRepo.FindBy(x => x.lesson_type == (byte)language && x.Id == (id ?? x.Id)).OrderBy(x => x.page_part).OrderBy(x => x.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = ex.Message.ToExceptionError();
            }
            return lesson;
        }

        public IPagedList<tblLessons> geneatePagelist(Enums.LessonLanguages language, int pageNumber)
        {
            var pageList = unitOfWork.tblLessonsRepo.FindBy(x => x.lesson_type == (byte)language).OrderBy(x => x.page_part).OrderBy(x => x.Id).ToPagedList(pageNumber, 1);
            return pageList;
        }
    }
}