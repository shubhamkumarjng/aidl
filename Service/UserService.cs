﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AIDL.Shared.Service;
using AIDL.Repository;
using AIDL.Models;
using System.Web.Security;

namespace AIDL.Service
{
    public class UserService : BaseService
    {
        public UserLogin userLoginModel;

        protected UnitOfWork unitOfWork
        { get; private set; }
        public UserService()
        {
            unitOfWork = new UnitOfWork();
        }

        public tblUsers LoginUser()
        {
            tblUsers user = new tblUsers();
            try
            {
                user = unitOfWork.tblUsersRepo.Find(x => x.email == userLoginModel.email);
                IsSuccess = false;
                if (user == null)
                {
                    Message = "Invalid username or password!".ToError();
                }
                else if (string.Compare(user.password, userLoginModel.password, false) != 0)
                {
                    Message = "Invalid username or password!".ToError();
                }
                else if (user.status == false)
                {
                    Message = "Your account is currently deactivted. Please contact customer care.".ToError();
                }
                else if (user.valid_upto < DateTime.Now.Date)
                {
                    Message = "Your account has been expired. Please contact customer care.".ToError();
                }
                else
                {
                    IsSuccess = true;
                }

                return user;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = ex.Message.ToSuccess();
            }
            return user;
        }
    }
}