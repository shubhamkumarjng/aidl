﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AIDL.Repository;
using AIDL.Shared.Service;

namespace AIDL.Service
{
    public class ContactUsService : BaseService
    {
        public tblContactUs contactUs { get; set; }

        protected UnitOfWork unitOfWork { get; private set; }
        public ContactUsService()
        {
            unitOfWork = new UnitOfWork();
        }

        public void sendContactUsEmail()
        {
            contactUs.CreatedOn = DateTime.Now;
            unitOfWork.tblContactUsRepo.Add(contactUs);
            unitOfWork.tblContactUsRepo.Save();
            //send mail to admin
            clsEmailTemplateService objEmailTemplateService = clsEmailTemplateService.Instance;
            System.Collections.Hashtable objHashTable = new System.Collections.Hashtable();
            objHashTable.Add("#name#", contactUs.FullName);
            objHashTable.Add("#carta#", contactUs.CartaDiIdentit);
            objHashTable.Add("#codice#", contactUs.CodiceFiscale);
            objHashTable.Add("#phone#", contactUs.Phone);
            objHashTable.Add("#email#", contactUs.Email);
            objHashTable.Add("#subject#", contactUs.Subject);
            objHashTable.Add("#message#", contactUs.Message);
            string body = objEmailTemplateService.GetEmailTemplate("contactus", objHashTable);
            (clsSendEmailService.Instance).SendEmail(new string[] { System.Configuration.ConfigurationManager.AppSettings["ClientMail"].ToString() }, null, null, body, "Enquiry");
            IsSuccess = true;
            Message = "We have received your enquiry and will respond to you within 24 hours.".ToSuccess();
        }
    }
}