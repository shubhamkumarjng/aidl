﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIDL.Models
{
    public class LessonModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string audio { get; set; }
        public string image { get; set; }
    }
}