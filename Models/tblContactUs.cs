﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AIDL.Repository
{
    [MetadataType(typeof(tblContactUsMetaData))]
    public partial class tblContactUs
    {
        private class tblContactUsMetaData
        {
            [Required(ErrorMessage = "Name is required")]
            public string FullName { get; set; }
            public string CartaDiIdentit { get; set; }
            public string CodiceFiscale { get; set; }
            [Required(ErrorMessage = "{0} is required")]
            public string Phone { get; set; }
            [Required(ErrorMessage = "{0} is required")]
            [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid email address")]
            public string Email { get; set; }
            public string Subject { get; set; }
            public string Message { get; set; }
        }
    }

}