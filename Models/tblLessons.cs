﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AIDL.Repository
{
    [MetadataType(typeof(tblLessonsMetaData))]
    public partial class tblLessons
    {

        [Required(ErrorMessage = "Lesson Audio is required")]
        public HttpPostedFileBase audio_file { get; set; }
        [Required(ErrorMessage = "Lesson Image is required")]
        public HttpPostedFileBase image_file { get; set; }

        private class tblLessonsMetaData
        {   
            [Required(ErrorMessage = "Lesson Type is required")]
            public byte lesson_type { get; set; }
            [Required(ErrorMessage = "Page Number is required")]
            public string page_number { get; set; }
            public Nullable<byte> page_part { get; set; }
            public string audio { get; set; }
            public string image { get; set; }
        }
    }

    
}