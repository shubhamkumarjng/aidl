﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AIDL.Repository
{
    [MetadataType(typeof(tblUsersMetaData))]
    public partial class tblUsers
    {
        private class tblUsersMetaData
        {
            [Required(ErrorMessage = "Name is required")]
            public string name { get; set; }
            [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid phone.")]
            public string phone { get; set; }
            [Required(ErrorMessage = "Email is required")]
            [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid email address")]
            [Remote("isEmailExists", "Users", "Admin",AdditionalFields ="Id")]
            public string email { get; set; }
            [MinLength(6, ErrorMessage = "Password must be minimum 6 character")]
            [MaxLength(50, ErrorMessage = "Password is maximum 50 characters long")]
            [Required(ErrorMessage = "Password is required")]
            public string password { get; set; }
            [Required(ErrorMessage = "Valid upto is required")]
            public DateTime valid_upto { get; set; }
        }
    }

}