﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AIDL.Models
{
    public class UserLogin
    {
        [Required(ErrorMessage = "Email is required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid email address")]
        public string email { get; set; }
        [MinLength(6, ErrorMessage = "Password must be minimum 6 character")]
        [MaxLength(50, ErrorMessage = "Password is maximum 50 characters long")]
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }
        public bool RememberMe { get; set; }
    }
}