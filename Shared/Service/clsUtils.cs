﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AIDL.Shared.Service
{
    public static class clsUtils
    {
        public static string FirstCharToUpper(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public static string GetAudioFilePath(string fileName, int languageEnumValue)
        {
            return $"/uploads/{Enums.GetLessonLanguages(languageEnumValue).ToLower()}/audio/{fileName}";
        }
        public static string GetImageFilePath(string fileName, int languageEnumValue)
        {
            return $"/uploads/{Enums.GetLessonLanguages(languageEnumValue).ToLower()}/image/{fileName}";
        }

    }
}