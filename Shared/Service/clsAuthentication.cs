﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace AIDL.Shared.Service
{
    public static class clsAuthentication
    {
        public static bool IsAuthenticated { get; private set; }
        public static int UserId { get; private set; }
        public static string UserEmail { get; private set; }
        public static string UserName { get; private set; }
        public static string UserRole { get; private set; }

        public static void checkUserAuthenticatin()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                clear();
                return;
            }
            if (!HttpContext.Current.User.IsInRole("User"))
            {
                clear();
                return;
            }
            HttpSessionStateBase Session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            FormsIdentity fi;
            fi = (FormsIdentity)HttpContext.Current.User.Identity;
            string[] ud = fi.Ticket.UserData.Split(';');
            IsAuthenticated = true;
            Session["UserRole"] = UserRole = ud[0].ToString();
            Session["UserId"] = UserId = Convert.ToInt32((string.IsNullOrEmpty(ud[1]) ? "0" : ud[1]));
            Session["UserName"] = UserName = ud[2];
            Session["UserUsername"] = UserEmail = ud[3];
        }
        private static void clear()
        {
            IsAuthenticated = false;
            UserId = 0;
            UserEmail = UserName = UserRole = "";
        }
        public static void SignOutUser()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
            IsAuthenticated = false;
            UserId = 0;
            UserEmail = UserName = UserRole = "";
            return;
        }

    }
}