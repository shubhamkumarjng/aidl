﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace AIDL.Shared.Service
{
    public sealed class clsSendEmailService : BaseService
    {
        private static readonly Lazy<clsSendEmailService> lazy =
        new Lazy<clsSendEmailService>(() => new clsSendEmailService());

        public static clsSendEmailService Instance { get { return lazy.Value; } }

        private clsSendEmailService() { }

        public void SendEmail(string[] to, string[] cc, string[] bcc, string body, string subject, string[] attachments = null)
        {
            try
            {
                subject = (Convert.ToString(ConfigurationManager.AppSettings["Environment"]).ToLower() == "live" ? "" : Convert.ToString(ConfigurationManager.AppSettings["Environment"]).ToLower() + " - ") + subject;
                MailMessage email = new MailMessage();

                SmtpClient smtp = new SmtpClient();

                // draft the email
                email.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"].ToString());
                if (to != null)
                {
                    foreach (string item in to)
                    {
                        email.To.Add(new MailAddress(item));
                    }
                }
                if (cc != null)
                {
                    foreach (string item in cc)
                    {
                        email.CC.Add(new MailAddress(item));
                    }
                }
                if (bcc != null)
                {
                    foreach (string item in bcc)
                    {
                        email.Bcc.Add(new MailAddress(item));
                    }
                }
                //every email bcc to bccEmails
                foreach (string bccEmail in ConfigurationManager.AppSettings["bccEmails"].ToString().Split(','))
                {
                    email.Bcc.Add(new MailAddress(bccEmail));
                }

                if (attachments != null)
                {
                    foreach (string attachment in attachments)
                    {
                        email.Attachments.Add(new Attachment(attachment));
                    }
                }
                email.Subject = subject;
                email.Body = body;
                email.IsBodyHtml = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                //smtp.Send(email);
                Task.Run(() => { SendEmailAsync(smtp, email); });
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                Message = "Error: " + ex.ToString();
            }
        }
        private async void SendEmailAsync(SmtpClient smtp, MailMessage email)
        {
            smtp.Send(email);
        }
    }
}