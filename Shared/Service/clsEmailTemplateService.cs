﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace AIDL.Shared.Service
{
    public class clsEmailTemplateService
    {
        private static readonly Lazy<clsEmailTemplateService> lazy =
        new Lazy<clsEmailTemplateService>(() => new clsEmailTemplateService());

        public static clsEmailTemplateService Instance { get { return lazy.Value; } }

        /// <summary>
        /// Define Template here and TemlateKey in a small characters
        /// </summary>
        /// <param name="TemplateKey"></param>
        /// <returns></returns>
        public StringBuilder GetTemplate(string TemplateKey)
        {
            StringBuilder sb = new StringBuilder();
            if (TemplateKey == "contactus")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                        <span style=""font-size: 14px;"">
                            Name: <strong>#name# </strong><br />
                            Carta di identit: <strong>#carta# </strong><br />
                            Codice fiscale: <strong>#codice# </strong><br />
                            Phone: <strong>#phone# </strong><br />
                            Email: <strong>#email# </strong><br />
                            Subject: <strong>#subject# </strong><br />
                            Message: <strong>#message# </strong><br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            //
            else if (TemplateKey == "signup")
            {
                sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
    <div>
        <p style='font-size: 16px; line-height:26px;font-family: gotham, helvetica, arial, sans-serif !important;'>
            <span style='font-size: 16px;'>
                <strong> #name# </strong><br />
                Thank you for signing up with Arora Italian Driving License.<br />
                If you have any questions, please reach us by email at: <a href='mailto:gurwinarora@gmail.com'>gurwinarora@gmail.com</a> or by phone at: (330) 194-0123.<br />
                To get started, please log in at <a href='http://www.AroraItalianDrivingLicense.com'> www.AroraItalianDrivingLicense.com </a> with your <strong>#username#</strong>  and <strong>#password#.</strong> <br />
                We look forward to helping you.
            </span><br />
            <span style='font-size:16px'>Sincerely,</span><br />
            <span style='font-size:16px'>The Arora Italian Driving License Staff</span>
        </p>
    </div>
</div>");
            }
            else if (TemplateKey == "recivedshipment")
            { sb.Append(@"
            <div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 16px; line-height:26px;"">
                        <span style=""font-size: 16px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                            Your shipment information was received by Arora Italian Driving License.<br />
                           <strong>#carrier#</strong><br />
                            <strong>#sender#</strong><br />
                            Tracking Number: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                            We will alert you when your shipment arrives.
                        </span><br />
                        <span style=""font-size:16px"">Sincerely,</span><br />
                        <span style=""font-size:16px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div> "); }
            else if (TemplateKey == "ordercreate")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                        <span style=""font-size: 14px;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                            Your order was created.  We will alert you when we have shipping information, including your tracking number.<br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderproccessing")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                            Your order is currently being processed.  Your reference number is:  <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderonhold")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                           Your order is currently on hold.  Your reference number is:  <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderunhold")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                          Your order has been released.  Your reference number is: <strong>#referenceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderabandon")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                          Your order has been abandoned or cancelled.  Your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "restricted")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your order is restricted and cannot be shipped.  Your reference number is: <strong>#referenceno#</strong><br />
                          <strong>Restricted reason:</strong> #Msg#  Click here to check  <a href=""http://www.AroraItalianDrivingLicense.com/privacy"">Privacy Policy</a> <br/>
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "invoicepricechanged")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your invoice price has changed.  Your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "invoicepricecancel")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your invoice price has been cancelled.  Your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "confirmorder")
            {
                sb.Append(@"<div style=""padding: 15px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your order is confirmed and is awaiting payment.  Your reference number is: <strong>#refernceno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "awaitingdispatch")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                        Your order is awaiting dispatch.  Your reference number is: <strong>#referenceno#</strong>  no and your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "dispatched")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                        Your order is dispatched.  Your reference number is: <strong>#referenceno#</strong>  no and your tracking number is: <strong>#trackingno#</strong><br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "orderpayment")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                          Your order has been dispatched.  Your reference number is: <strong>#referenceno#</strong>  no and your tracking number is: <strong>#trackingno#</strong>  Your invoice is attached for your records.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }

            else if (TemplateKey == "shipmentreturnaccept")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your shipment return has been accepted.  Your tracking number is: <strong>#trackingno#</strong>.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "shipmentreturnreject")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         Your shipment return has been rejected.  Your tracking number is: <strong>#trackingno#</strong>.<br />
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "pictureuploaded")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                         A picture of your order has been uploaded. Please sign in to <a href=""http://www.AroraItalianDrivingLicense.com"">www.AroraItalianDrivingLicense.com</a> for more details.<br />
                          Your tracking number is: <strong>#trackingno#</strong>.</br>
                            If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "refferalinvitation")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #name#</strong><br />
                        You have been invited by #Useremail# to try  <a href=""http://www.AroraItalianDrivingLicense.com""> www.AroraItalianDrivingLicense.com </a>.<br />
                        <a href=""#Links#"">Click here</a> avail your discount Signup fees <strong>#ReferalDiscount#</strong>%<br/>
                        To start shopping, shipping, and saving money on your international orders, visit our page.</br>
                           If you would like to continue to receive communication from us, please visit our newsletter sign up at: <a href=""http://www.AroraItalianDrivingLicense.com/newsletter""> www.AroraItalianDrivingLicense.com/newsletter</a><br />
                         We look forward to delivering your favorite US items right to your door soon.
                        </span><br />

                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }

            else if (TemplateKey == "forgotpassword")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong>#email#</strong><br />
                           <strong>#recoverpasswored# </strong> <br />
                         You have recently let us know that you forgot your password.  Please log in at www.AroraItalianDrivingLicense.com to update your password.<br/>
                         If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "refersignup")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong> #FirstName#  #LastName# </strong><br />
                          You have received reward points for referring a new customer.  To show our thanks, we have added <strong>#rewardpoint# </strong> to your account.<br />
                          If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "rewardpoint")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong> #FirstName#  #LastName# </strong><br />
                         You currently have reward points: <strong>#rewardpoints#</strong><br />
                          If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "apierroremail")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong>Shipping Api Error</strong><br />
                             <strong>Carrier Name :</strong> #CarrierName#<br/>
                             <strong>Carrier Services :</strong> #CarrierServices# <br/>
                               <strong>User :</strong>  #UserName#<br/>
                               <strong>Order RefNo :</strong> #OrderRefernceNo#<br/>
                                <strong>Message :</strong> #Msg#<br/>
                                <strong>Date :</strong>#Date#
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "notificationemail")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                            <strong> #FirstName#  #LastName# </strong><br />
                            Tracking Number: <strong>#tracking#</strong><br />
                            Carrier: <strong>#carrier#</strong><br />
                            Sender: <strong>#sender#</strong><br />
                             Message:  <strong>#Msg#</strong><br />
                            If you have any questions about your shipment, please contact us by email at:<a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "paymentfailed")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
                <div>
                    <p style=""font-size: 13px; line-height:26px;"">
                        <span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
                           <strong> #FirstName#  #LastName# </strong><br />
                         Payment failed :<strong>#failedMsg#</strong><br />
                        </span><br />
                        <span style=""font-size:15px"">Sincerely,</span><br />
                        <span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
                    </p>
                </div>
            </div>");
            }
            else if (TemplateKey == "customproforma_template")
            {
                sb.Append(@"
<html lang=""en"">
<body>
    <div class=""container"">
        <table class=""tbl1"">
            <tr>
                <th class=""h1 noborder"">
                    Proforma Invoice
                </th>
                <th class=""noborder"">
                </th>
            </tr>
            <tr>
                <th class=""noborder"">
                    Date: #invoicedate#
                </th>
                <th class=""noborder"">
                    CARRIER: #carrier#
                </th>
            </tr>
        </table>
        <table cellpadding=""5""class=""tbl2"">
            <tr>
                <th class=""underline"">
                    <b>SENT BY</b>
                </th>
                <th class=""underline"">
                    <b>SENT TO</b>
                </th>
            </tr>
            <tr>
                <td>
                    Name: <strong>#byname#</strong>
                </td>
                <td>
                    Name: <strong>#toname#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Address: <strong>#byaddress#</strong>
                </td>
                <td>
                    Address: <strong>#toaddress#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    City/Postal Code: <strong>#bycity#</strong>
                </td>
                <td>
                    City/Postal Code: <strong>#tocity#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Country: <strong>#bycountry#</strong>
                </td>
                <td>
                    Country: <strong>#tocountry#</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Telephone/Fax: <strong>#bytelephone#</strong>
                </td>
                <td>
                    Telephone/Fax: <strong>#totelephone#</strong>
                </td>
            </tr>
        </table>
        <table cellpadding=""5"" id=""desc"" class=""tbl3"">
            <thead>
                <tr>
                    <th>
                        <b>FULL DESCRIPTION OF GOODS</b>
                    </th>
                    <th>
                        <b>ORIGIN COUNTRY</b>
                    </th>
                    <th>
                        <b>CUSTOMS COMMODITY CODE</b>
                    </th>
                     <th>
                        <b>HARMONIZED CODE</b>
                    </th>
                    <th>
                        <b>QUANTITY</b>
                    </th>
                    <th>
                        <b>UNIT VALUE AND CURRENCY</b>
                    </th>
                    <th>
                        <b>TOTAL VALUE AND CURRENCY</b>
                    </th>
                </tr>
            </thead>
            <tbody style=""text-align:center;"">
                        #shipmentdetail#
                <tr>
                    <td colspan=""4"" style=""text-align:right;"">
                        TOTAL VALUE AND CURRENCY
                    </td>
                    <td colspan=""2"" style=""text-align:center;"">
                        #demototal#
                    </td>
                </tr>
            </tbody>
        </table>
        <table class=""noborder tbl4""cellpadding=""5"">
            <tr class=""noborder"">
                <th class=""noborder colspn"">
                    <b>Value for customs purposes only.</b>
                </th>
            </tr>
            <tr class=""noborder"">
                <th class=""noborder colspn"">
                    <b>No commercial value.</b>
                </th>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    REASON FOR EXPORT:
                </td>
                <td class=""noborder"">
                    #reasonforexport#
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    TERMS OF DELIVERY(INCOTERMS 2000) :
                </td>
                <td class=""noborder"">
                    #termsofdelivery#
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    NUMBER AND KIND OF PACKAGES :
                </td>
                <td class=""noborder"">
                    #numberandkindpackage#
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    NET WEIGHT :
                </td>
                <td class=""noborder"">
                    #billableweight# (LB)
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    <strong>Place and date :</strong>
                </td>
                <td class=""noborder"">
                    #placeanddate#
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    <strong>Name:</strong>
                </td>
                <td class=""noborder"">
                    #name#
                </td>
            </tr>
            <tr class=""noborder"">
                <td class=""noborder"">
                    <strong>Signature :</strong>
                </td>
                <td class=""noborder"">
                    #signature#
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
");
            }
            else if (TemplateKey == "psorderpayment")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
	<div>
		<p style=""font-size: 13px; line-height:26px;"">
			<span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
				<strong> #FirstName#  #LastName# </strong><br />
				Your personal shopper order with reference number : <strong>#referenceno#</strong> has been created successfully. #InvoiceAttachLine#<br />
				If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
			</span><br />
			<span style=""font-size:15px"">Sincerely,</span><br />
			<span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "psorderpaymnetAdmin")
            {
                sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
	<div>
		<p style='font-size: 13px; line-height:26px;'>
			<span style='font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;'>
				<strong> Dear Admin </strong><br />
				A new personal shopper order with reference number : <string>#referenceno#</string> has been logged.<br />

			</span><br />
			<span style='font-size:15px'>Sincerely,</span><br />
			<span style='font-size:15px'>The Arora Italian Driving License Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "psordercanceled")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
	<div>
		<p style=""font-size: 13px; line-height:26px;"">
			<span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
				<strong> #FirstName#  #LastName# </strong><br />
				Your personal shopper order with reference number : <strong>#referenceno#</strong> has been canceled successfully.<br />
				If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
			</span><br />
			<span style=""font-size:15px"">Sincerely,</span><br />
			<span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "psitemcanceled")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
	<div>
		<p style=""font-size: 13px; line-height:26px;"">
			<span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
				<strong> #FirstName#  #LastName# </strong><br />
				Your personal shopper item with item number : <strong>#itemno#</strong> which is in a order with reference number : <strong>#referenceno#</strong> has been canceled successfully.<br />
				If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
			</span><br />
			<span style=""font-size:15px"">Sincerely,</span><br />
			<span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "psordercompleted")
            {
                sb.Append(@"<div style=""padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;"">
	<div>
		<p style=""font-size: 13px; line-height:26px;"">
			<span style=""font-size: 14px; font-family: gotham, helvetica, arial, sans-serif !important;"">
				<strong> #FirstName#  #LastName# </strong><br />
				Your personal shopper order with reference number : <strong>#referenceno#</strong> has been completed successfully.<br />
				If you have any questions about your shipment, please contact us by email at: <a href=""mailto:gurwinarora@gmail.com"">gurwinarora@gmail.com</a>  or by phone at: (330) 194-0123.<br />
			</span><br />
			<span style=""font-size:15px"">Sincerely,</span><br />
			<span style=""font-size:15px"">The Arora Italian Driving License Staff</span>
		</p>
	</div>
</div>");
            }
            else if (TemplateKey == "signupinvoice")
            {
                sb.Append(@"
<table class=""topTable"">
	<tr>
		<td class=""imgDiv"" style=""width:30%"">
			<img height=""35px"" src=""http://www.AroraItalianDrivingLicense.com/Content/img/logo.png"" />
		</td>
		<td class=""divH1"" style=""width:30%"">
			<h1 class=""h1"">
				Invoice
			</h1>
		</td>
		<td class=""tdRight"" style=""width:30%"">
			<b>Invoice Number :</b>  #invoicenumber#<br />
			<b>Date :</b>#invoicedate#<br />
		</td>
	</tr>
	<tr>
		<td align=""right"">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align=""left"" valign=""bottom"">
			<b>From Address :</b>
			<br />
            #fromAddress1#<br />
            #fromAddress2#<br />
            #fromcity#,#fromstate#<br />
            #fromzipcode#<br />
            #fromcountry#<br />
            #fromphoneno#
		</td>
		<td align=""left"">
			&nbsp;
		</td>
		<td class=""tdRight"">
            <b>To Address :</b><br />
            #firstName#  #lastName#<br />
            #address1#<br />
            #address2#
            #city#,#state#<br />
            #postalcode#<br />
            #country#<br />
            #mobileNo#<br />
		</td>
	</tr>
	<tr>
		<td colspan=""4"" class=""tdH2"">
			<h2 class=""h2"">
				Invoice Details
			</h2>
		</td>
	</tr>
	<tr>
		<td align=""left"" valign=""top"" colspan=""3"">
			<table cellpadding=""12"" id=""desc"" style=""width: 100%; text-align: left; border: 1px solid black;"">
				<thead>
					<tr align=""left"" valign=""top"">
						<th width=""30%"">
							Card No
						</th>
						<th width=""30%"">
						Expire Date
						</th>
						<th width=""30%"">
							Pay Amount
						</th>
						<th width=""25%"">
							Plan
						</th>
					</tr>
					<tr align=""left"" valign=""top"" style=""font-weight: bold;"">
						<td>
                            #cardno#
						</td>
						<td>
                            #date#
						</td>
						<td>
                            $ #paymentamount#
						</td>
						<td>
                            #plan#
						</td>
					</tr>
					<tr>
						<td align=""left"" valign=""top"" colspan=""6"">
							<br />
							<center>
								<span style=""font-size: larger;"">
									Thank you for using Arora Italian Driving License.com, we appreciate your
									experience with us.
								</span>
							</center>
							<br />
							<br />
							*These charges have been raised based on the size and weight declared for these
							shipments. If the carrier advises that the size or weight is larger then additional
							carriage and administration charges will be due. If additional services post collection
							are provided by the carrier for these shipments additional charges may be due in
							accordance with our terms and conditions.
							<br />
							<br />
							<center>
								<span style=""font-size: small;"">
									Arora Italian Driving License.com is a trading style of Fili Imports, registered
									in USA No. LCC_Number_Here
								</span>
							</center>
						</td>
					</tr>
					<tr>
						<td align=""left"" valign=""top"" colspan=""3"">
							&nbsp;
						</td>
					</tr>
				</thead>
			</table>
		</td>
	</tr>
</table>
");
            }
            else if (TemplateKey == "orderinvoice")
            {
                sb.Append(@"
<table class=""topTable"">
    <tr>
        <td class=""imgDiv"">
            <img height=""35px"" src=""http://www.AroraItalianDrivingLicense.com/Content/img/logo.png"" />
        </td>
        <td class=""divH1"">
            <h1 class=""h1"">
                Invoice
            </h1>
        </td>
        <td class=""tdRight"">
            <b>Invoice Number :</b>#invoicno#<br />
            <b>Date :</b>#invoicedate#<br />
        </td>
    </tr>
    <tr>
        <td align=""right"">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align=""left""valign=""bottom"">
            <b>From Address :</b> <br />
            Arora Italian Driving License<br />
            2115 S Union Ave<br />
            Suite #suite#<br />
            Alliance,OH<br />
            44601<br />
            United States<br />
            3305741906
        </td>
        <td align=""left"">
            &nbsp;
        </td>
        <td class=""tdRight"">
            <b>To Address :</b><br />
            #toname#<br />
            #toaddress#<br />   
            #toaddress2#
            #tocity#,#tostate#<br />
            #tozip#<br />
            #tocountry#<br />
            #totelephone#<br />
        </td>
    </tr>
    <tr>
        <td colspan=""4"" class=""tdH2"">
            <h2 class=""h2"">
                Order Details
            </h2>
        </td>
    </tr>
    <tr>
        <td align=""left"" valign=""top""colspan=""3"">
            <table cellpadding=""12"" id=""desc"" class=""Percent100"" style=""text-align:center; border:1px solid black;font-size:small"">
                <thead>
                    <tr align=""left"" valign=""top"" style=""font-size:small"">
                        <th>
                            Order Ref No
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Tracking No
                        </th>
                        <th>
                            Package(s)
                        </th>
                        <th>
                            Consolidate Shipment(s)
                        </th>
                        <th>
                            Pay Amount
                        </th>
                        <th>
                            Wallet Consume Amount
                        </th>
                    </tr>
                    <tr align=""left"" valign=""top"">
                        <td>
                            #ordrefno#
                        </td>
                        <td>
                            #date#
                        </td>
                        <td>
                            #trackingno#
                        </td>
                        <td>
                            #packages#
                        </td>
                        <td>
                            #shipments#
                        </td>
                        <td>
                            US$ #amount#
                        </td>
                        <td>
                            US$ #wallet_amount#
                        </td>
                    </tr>
                    <tr>
                        <td style=""text-align:right"" valign='top' colspan='4'>
                            <b>Invoice Total</b>
                        </td>
                        <td  style=""text-align:left"" valign='top' colspan='3'>
                            US$ #invoicetotal#
                        </td>
                    </tr>
                    <tr>
                        <td align=""left"" valign=""top"" colspan=""7"">
                            <br />
                            <center>
                                <span style=""font-size: larger;"">
                                    Thank you for using Arora Italian Driving License.com, we appreciate your
                                    experience with us.
                                </span>
                            </center>
                            <br />
                            <br />
                            *These charges have been raised based on the size and weight declared for these
                            shipments. If the carrier advises that the size or weight is larger then additional
                            carriage and administration charges will be due. If additional services post collection
                            are provided by the carrier for these shipments additional charges may be due in
                            accordance with our terms and conditions.
                            <br />
                            <br />
                            <center>
                                <span style=""font-size: small;"">
                                    Arora Italian Driving License.com is a trading style of Fili Imports, registered
                                    in USA No. LCC_Number_Here
                                </span>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td align=""left"" valign=""top"" colspan=""3"">
                            &nbsp;
                        </td>
                    </tr>
                </thead>
            </table>
        </td>
    </tr>
</table>");
            }
            else if (TemplateKey == "psorderinvoice")
            {
                sb.Append(@"<table class=""topTable"">
	<tr>
		<td class=""imgDiv"">
			<img height=""35px"" src=""http://www.AroraItalianDrivingLicense.com/Content/img/logo.png"" />
		</td>
		<td class=""divH1"">
			<h1 class=""h1"">
				Invoice
			</h1>
		</td>
		<td class=""tdRight"">
			<b>Invoice Number :</b>#invoicno#<br />
			<b>Date :</b>#invoicedate#<br />
		</td>
	</tr>
	<tr>
		<td align=""right"">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align=""left"" valign=""bottom"">
			<b>From Address :</b> <br />
			Arora Italian Driving License<br />
			2115 S Union Ave<br />
			Suite #suite#<br />
			Alliance,OH<br />
			44601<br />
			United States<br />
			3305741906
		</td>
		<td align=""left"">
			&nbsp;
		</td>
		<td class=""tdRight"">
			<b>To Address :</b><br />
			#toname#<br />
			#toaddress#<br />
			#tocity#,#tostate#<br />
			#tozip#<br />
			#tocountry#<br />
			#totelephone#<br />
		</td>
	</tr>
	<tr>
		<td colspan=""4"" class=""tdH2"">
			<h2 class=""h2"">
				Personal Shopper Order Detail
			</h2>
		</td>
	</tr>
	<tr>
		<td align=""left"" valign=""top"" colspan=""3"">
			<table cellpadding=""12"" id=""desc"" class=""Percent100"" style=""text-align:center; border:1px solid black;font-size:small;width:100%"">
				<thead>
					<tr align=""left"" valign=""top"" style=""font-size:small"">
						<th>
							PS Order Ref No
						</th>
						<th>
							Date
						</th>
						<th>
							Merchant
						</th>
						<th>
							No. of Item(s)
						</th>
						<th>
							Pay Amount
						</th>
						<th >
							Wallet Consume Amount
						</th>
					</tr>
					<tr>
						<td>
							#ordrefno#
						</td>
						<td>
							#date#
						</td>
						<td>
							#merchant#
						</td>
						<td>
							#items_count#
						</td>
						<td>
							US$ #amount#
						</td>
						<td>
							US$ #wallet_amount#
						</td>
					</tr>
					<tr>
						<td style=""text-align:right"" valign=""top"" colspan=""4"">
							<b>Invoice Total</b>
						</td>
						<td style=""text-align:left"" valign=""top"" colspan=""2"">
							US$ #invoicetotal#
						</td>
					</tr>
					<tr>
						<td align=""left"" valign=""top"" colspan=""6"">
							<br />
							<center>
								<span style=""font-size: larger;"">
									Thank you for using Arora Italian Driving License.com, we appreciate your
									experience with us.
								</span>
							</center>
							<br />
							<br />
							<center>
								<span style=""font-size: small;"">
									Arora Italian Driving License.com is a trading style of Fili Imports, registered
									in USA No. LCC_Number_Here
								</span>
							</center>
						</td>
					</tr>
					<tr>
						<td align=""left"" valign=""top"" colspan=""3"">
							&nbsp;
						</td>
					</tr>
				</thead>
			</table>
		</td>
	</tr>
</table>");

            }
            else if (TemplateKey == "affiliate_signup")
            {
                sb.Append(@"<div style='padding: 15px;font-family: gotham, helvetica, arial, sans-serif !important;'>
    <div>
        <p style='font-size: 16px; line-height:26px;font-family: gotham, helvetica, arial, sans-serif !important;'>
            <span style='font-size: 16px;'>
                <strong> Dear Affiliate User, </strong><br /><br />
                Thank you for signing up with Arora Italian Driving License affiliate program.<br /><br />
                Your affilate link is : <strong><a href='#affiliatlink#' target='_blank'>#affiliatlink#</a></strong><br />
                To get started, please log in at <a href='http://www.AroraItalianDrivingLicense.com/affiliate-sign-in'> www.AroraItalianDrivingLicense.com/affiliate-sign-in </a> .
                If you have any questions, please reach us by email at: <a href='mailto:gurwinarora@gmail.com'>gurwinarora@gmail.com</a> or by phone at: (330) 194-0123.<br />
                You may also visit our <a href='http://www.AroraItalianDrivingLicense.com/faqs'> FAQs</a> and <a href='http://www.AroraItalianDrivingLicense.com/termsandconditions'>Terms and Services</a>  for more information.<br />
                We look forward to helping you shop, ship, and save money with our forwarding and consolidation services.
            </span><br /><br />
            <span style='font-size:16px'>Sincerely,</span><br />
            <span style='font-size:16px'>The Arora Italian Driving License Staff</span>
        </p>
    </div>
</div>");
            }
            if (TemplateKey == "signupinvoice" || TemplateKey == "customproforma_template" || TemplateKey == "orderinvoice" || TemplateKey == "psorderinvoice")
            {
                return sb;
            }

            string HeaderTemplatedtstring = HeaderTemplate();
            string FooterTemplatestring = FooterTemplate();
            StringBuilder AppendTempalte = new StringBuilder(HeaderTemplatedtstring + sb + FooterTemplatestring);
            return AppendTempalte;
        }

        public string GetEmailTemplate(string TemplateKey, Hashtable TemplateData)
        {
            try
            {

                StringBuilder FormTempleate = GetTemplate(TemplateKey.ToLower());
                foreach (DictionaryEntry d in TemplateData)
                {
                    FormTempleate = FormTempleate.Replace(d.Key.ToString(), Convert.ToString(d.Value));
                }
                return FormTempleate.ToString();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static string HeaderTemplate()
        {
            return @"<html><head>
   <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
</head>
<body>
    <div style=""float: left;margin: 53px 0 0;width: 100%;"">
        <div style=""background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
            margin: auto;width: 90%;border-color: #517FBB !important; min-height: 239px !important; border-radius:none!important;"">
            <div style=""border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;border-bottom-color:#517FBB!important; padding: 15px 10px;background-color: #E5E5E5 !important;font-size:15px;"">
                <img src=""http://www.aroraitaliandrivinglicense.com/content/img/logo-small.png"" height=""35px""/>
            </div>";
        }

        public static string FooterTemplate()
        {
            return @"</div>  </div> </body ></html>";
        }
    }
}